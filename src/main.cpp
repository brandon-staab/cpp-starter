#include "print_args.hpp"

#include <spdlog/spdlog.h>


[[nodiscard]] int
main([[maybe_unused]] int const p_argc, [[maybe_unused]] char const* const* const p_argv) noexcept {
	spdlog::info("Hello World!");
	print_args(std::span{p_argv, static_cast<std::size_t>(p_argc)});
	spdlog::info("Program ran successfully.");
}

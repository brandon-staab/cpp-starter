#include "print_args.hpp"

#include <iostream>


void
print_args(std::span<char const* const> p_args) {
	for (auto i{0U}; i < p_args.size(); i++) {
		std::clog << "args[" << i << "] = " << p_args[i] << '\n';
	}
}

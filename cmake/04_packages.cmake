add_library("project_packages" INTERFACE)

# Boost
find_package("Boost" 1.72.0 REQUIRED
	COMPONENTS
		"random"
)
target_link_libraries("project_packages"
	INTERFACE
		"Boost::headers"
		"Boost::random"
)

# Threads
set("CMAKE_THREAD_PREFER_PTHREAD" TRUE)
set("THREADS_PREFER_PTHREAD_FLAG" TRUE)
find_package("Threads" REQUIRED)
target_link_libraries("project_packages"
	INTERFACE
		"Threads::Threads"
)

# FMT
find_package("fmt" REQUIRED)
target_link_libraries("project_packages"
	INTERFACE
		"fmt::fmt"
)

# Spdlog
## Use headers only until clang-tidy warnings fixed
#find_package("spdlog" REQUIRED)
#target_link_libraries("project_packages"
#	INTERFACE
#		"spdlog::spdlog"
#)

include("src/sources.cmake")
add_library("project_core" "${CORE_SOURCE}")

target_compile_features("project_core"
	PUBLIC
		"cxx_std_20"
)

target_link_libraries("project_core"
	PUBLIC
		"project_warnings"
		"project_packages"
)

target_include_directories(project_core
	PUBLIC
		"${CMAKE_SOURCE_DIR}"
		"${CMAKE_SOURCE_DIR}/include"
)

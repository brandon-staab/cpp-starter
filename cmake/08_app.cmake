add_executable("app" "../src/main.cpp")

set_target_properties("app"
	PROPERTIES
		RUNTIME_OUTPUT_DIRECTORY
			"${CMAKE_CURRENT_BINARY_DIR}/bin"
)

target_link_libraries("app"
	LINK_PUBLIC
		"project_core"
)

#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

pacman -Syuq --noconfirm --needed \
  gcc                    \
  clang                  \
  make                   \
  cmake                  \
  git                    \
  graphviz               \
  doxygen                \
  \
  boost                  \
  fmt                    \
  spdlog

rm -rf dependencies &&
mkdir dependencies &&
cd dependencies

function install_from_git() {
	git clone "$1" &&
	cd "${1##*/}" &&
	mkdir -p build &&
	cd build &&
	cmake .. &&
	make install &&
	cd ../../.
}

# install_from_git https://github.com/.../...

cd ..
rm -rf dependencies

cpp-starter [![pipeline status](https://gitlab.com/brandon-staab/cpp-starter/badges/master/pipeline.svg)](https://gitlab.com/brandon-staab/cpp-starter/commits/master)
===============================

What is this?
-------------------------------
A C++ CMake project template.

Requirements
-------------------------------
```shell
./dependencies.sh
```

* C++ 20

Setting up build environment
-------------------------------
```shell
mkdir -p build
cd build
cmake ..
```

Running
-------------------------------
```shell
make run args="arg1 arg2 arg3"
```

Documentation
-------------------------------
### [README.md](https://gitlab.com/brandon-staab/cpp-starter/-/blob/master/README.md)

### Doxygen
```shell
make docs
```

Testing
-------------------------------
```shell
make check
```

Debugging
-------------------------------
```shell
make debug
```
